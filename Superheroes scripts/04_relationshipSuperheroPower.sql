CREATE TABLE SuperheroPower(
	SuperheroId integer NOT NULL,
	PowerId integer NOT NULL
);

ALTER TABLE SuperheroPower ADD CONSTRAINT FK_Superhero FOREIGN KEY (SuperheroId) REFERENCES Superhero(Id);
ALTER TABLE SuperheroPower ADD CONSTRAINT FK_Power FOREIGN KEY (PowerId) REFERENCES HeroPower(Id);
ALTER TABLE SuperheroPower ADD PRIMARY KEY(SuperheroId, PowerId);
