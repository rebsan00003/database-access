CREATE TABLE Superhero(
	Id integer NOT NULL IDENTITY(1,1),
	HeroName varchar(150),
	Alias varchar(30),
	Origin varchar(150),
	PRIMARY KEY (Id)
);

CREATE TABLE Assistant(
	Id integer NOT NULL IDENTITY(1,1),
	AssistantName varchar(150),
	PRIMARY KEY (Id)
);

CREATE TABLE HeroPower(
	Id integer NOT NULL IDENTITY(1,1),
	PowerName varchar(150),
	PowerDescription varchar(150),
	PRIMARY KEY (Id)
);

