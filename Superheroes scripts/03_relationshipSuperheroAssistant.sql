ALTER TABLE Assistant ADD SuperheroId integer;
ALTER TABLE Assistant ADD CONSTRAINT FK_Superhero FOREIGN KEY (SuperheroId) REFERENCES Superhero(Id);
