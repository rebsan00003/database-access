INSERT INTO HeroPower (PowerName, PowerDescription) VALUES ('Intelligence', 'Having genius-level intelligence.');
INSERT INTO HeroPower (PowerName, PowerDescription) VALUES ('Superhuman strength', 'The ability to lift weights beyond what is physically possible for an ordinary human being.');
INSERT INTO HeroPower (PowerName, PowerDescription) VALUES ('Flight', 'The abiility to fly at high speeds without any outside influence.');
INSERT INTO HeroPower (PowerName, PowerDescription) VALUES ('Wallcrawling', 'The ability to scale walls and ceilings no matter how rough or smooth they are.');

--Batman gets Intelligence
INSERT INTO SuperheroPower (SuperheroID, PowerId) VALUES (1, 1);
--Wonder woman gets superhuman strength and flight
INSERT INTO SuperheroPower (SuperheroID, PowerId) VALUES (2, 2);
INSERT INTO SuperheroPower (SuperheroID, PowerId) VALUES (2, 3);
--Spiderman gets wallcrawling and superhuman strength
INSERT INTO SuperheroPower (SuperheroID, PowerId) VALUES (3, 4);
INSERT INTO SuperheroPower (SuperheroID, PowerId) VALUES (3, 2);

