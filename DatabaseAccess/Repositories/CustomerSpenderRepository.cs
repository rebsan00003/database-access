﻿using DatabaseAccess.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseAccess.Repositories
{
    class CustomerSpenderRepository : ICustomerSpenderRepository
    {
        /// <summary>
        /// Get all customers in order of highest spender, in descending order
        /// </summary>
        /// <returns>A list of CustomerSpender instances</returns>
        public List<CustomerSpender> GetHighestSpenders()
        {
            List<CustomerSpender> custSpenders = new List<CustomerSpender>();
            string sql = "SELECT Customer.CustomerId, ISNULL(Customer.FirstName, 'Empty'), ISNULL(Customer.LastName, 'Empty'), ISNULL(Customer.Country, 'Empty'), ISNULL(Customer.PostalCode, 'Empty'), ISNULL(Customer.Phone, 'Empty'), ISNULL(Customer.Email, 'Empty'),  SUM(Invoice.Total) AS TotalSpend FROM Customer, Invoice " +
            "WHERE Customer.CustomerId = Invoice.CustomerId GROUP BY Customer.CustomerId, Customer.FirstName, Customer.LastName, Customer.Country, Customer.PostalCode, Customer.Phone, Customer.Email " +
            "ORDER BY SUM(Invoice.Total) DESC";

            try
            {
                //Connect
                using (SqlConnection conn = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    conn.Open();
                    //Make a command
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        //Reader
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                //Handle result
                                CustomerSpender temp = new CustomerSpender();
                                temp.Customer = new Customer() { 
                                    CustomerId = reader.GetInt32(0), 
                                    FirstName = reader.GetString(1), 
                                    LastName = reader.GetString(2), 
                                    Country = reader.GetString(3), 
                                    PostalCode = reader.GetString(4), 
                                    Phone = reader.GetString(5), 
                                    Email = reader.GetString(6) 
                                };
                                temp.TotalSpend = reader.GetDecimal(7);
                                custSpenders.Add(temp);
                            }
                        }
                    }
                }

            }
            catch (SqlException ex)
            {
                Console.WriteLine("Problem with accessing data from database: " + ex);
            }

            return custSpenders;
        }
    }
}
