﻿using DatabaseAccess.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseAccess.Repositories
{
    class CustomerGenreRepository : ICustomerGenreRepository
    {
        /// <summary>
        /// Get a customer's most popular genre by using the customer's id as a parameter
        /// </summary>
        /// <param name="id"></param>
        /// <returns>A list of CustomerGenre instances</returns>
        public List<CustomerGenre> GetMostPopularGenre(int id)
        {
            List<CustomerGenre> customerGenres = new List<CustomerGenre>();
            string sql = "SELECT TOP 1 WITH TIES Genre.Name, COUNT(Genre.GenreId) AS NumberOfTracks FROM Genre " +
                            "INNER JOIN Track ON Track.GenreId = Genre.GenreId " +
                            "INNER JOIN InvoiceLine ON InvoiceLine.TrackId = Track.TrackId " +
                            "INNER JOIN Invoice ON Invoice.InvoiceId = InvoiceLine.InvoiceId " +
                            "INNER JOIN Customer ON Customer.CustomerId = Invoice.CustomerId WHERE Customer.CustomerId = @CustomerId " +
                            "GROUP BY Customer.CustomerId, Genre.Name " +
                            "ORDER BY COUNT(Genre.GenreId) DESC ";

            try
            {
                //Connect
                using (SqlConnection conn = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    conn.Open();
                    //Make a command
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        cmd.Parameters.AddWithValue("@CustomerID", id);
                        //Reader
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                //Handle result
                                CustomerGenre temp = new CustomerGenre();
                                temp.GenreName = reader.GetString(0);
                                temp.NumberOfTracks = reader.GetInt32(1);
                                customerGenres.Add(temp);
                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine("Problem with accessing data from database: " + ex);
            }
            return customerGenres;
        }
    }
}

