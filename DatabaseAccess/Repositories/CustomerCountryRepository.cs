﻿using DatabaseAccess.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseAccess.Repositories
{
    class CustomerCountryRepository : ICustomerCountryRepository
    {
        /// <summary>
        /// Get number of customers per cuntry in descending order 
        /// </summary>
        /// <returns>List of CustomerCountry instances</returns>
        public List<CustomerCountry> GetCustomerCountries()
        {
            List<CustomerCountry> custCountryList = new List<CustomerCountry>();
            string sql = "SELECT COUNT(CustomerId), Country FROM Customer GROUP BY Country ORDER BY COUNT(CustomerId) DESC";

            try
            {
                //Connect
                using (SqlConnection conn = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    conn.Open();
                    //Make a command
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        //Reader
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                //Handle result
                                CustomerCountry temp = new CustomerCountry();
                                temp.NoOfCustomers = reader.GetInt32(0);
                                temp.CountryName = reader.GetString(1);
                                custCountryList.Add(temp);
                            }
                        }
                    }
                }

            }
            catch (SqlException ex)
            {
                Console.WriteLine("Problem with accessing data from database: "+ ex);
            }

            return custCountryList;
        }
    }
}
