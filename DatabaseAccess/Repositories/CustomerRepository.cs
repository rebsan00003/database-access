﻿using DatabaseAccess.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseAccess.Repositories
{
    public class CustomerRepository : ICustomerRepository
    {
        /// <summary>
        /// Get all customers from the customer table
        /// </summary>
        /// <returns>A list of customers</returns>
        public List<Customer> GetAllCustomers()
        {
            List<Customer> custList = new List<Customer>();
            string sql = "SELECT CustomerId, ISNULL(FirstName, 'Empty'), ISNULL(LastName, 'Empty'), ISNULL(Country, 'Empty'), ISNULL(PostalCode, 'Empty'), ISNULL(Phone, 'Empty'), ISNULL(Email, 'Empty') FROM Customer";
            
            try
            {
                //Connect
                using (SqlConnection conn = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    conn.Open();
                    //Make a command
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        //Reader
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                //Handle result
                                Customer temp = new Customer();

                                temp.CustomerId = reader.GetInt32(0);
                                temp.FirstName = reader.GetString(1);
                                temp.LastName = reader.GetString(2);
                                temp.Country = reader.GetString(3);
                                temp.PostalCode = reader.GetString(4);
                                temp.Phone = reader.GetString(5);
                                temp.Email = reader.GetString(6);
                                custList.Add(temp);
                            }
                        }
                    }
                }

            }
            catch (SqlException ex)
            {
                Console.WriteLine("Problem with accessing data from database: " + ex);
            }

            return custList;
            
        }

        /// <summary>
        /// Get a specific customer by customerId as a parameter
        /// </summary>
        /// <param name="id"></param>
        /// <returns>A customer</returns>
        public Customer GetCustomerByID(int id)
        {
            Customer customer = new Customer();
            string sql = "SELECT CustomerId, ISNULL(FirstName, 'Empty'), ISNULL(LastName, 'Empty'), ISNULL(Country, 'Empty'), ISNULL(PostalCode, 'Empty'), ISNULL(Phone, 'Empty'), ISNULL(Email, 'Empty') FROM Customer" + 
                " WHERE CustomerId = @CustomerID";
            
            try
            {
                //Connect
                using (SqlConnection conn = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    conn.Open();
                    //Make a command
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        cmd.Parameters.AddWithValue("@CustomerID", id);
                        //Reader
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                //Handle result
                                customer.CustomerId = reader.GetInt32(0);
                                customer.FirstName = reader.GetString(1);
                                customer.LastName = reader.GetString(2);
                                customer.Country = reader.GetString(3);
                                customer.PostalCode = reader.GetString(4);
                                customer.Phone = reader.GetString(5);
                                customer.Email = reader.GetString(6);

                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine("Problem with accessing data from database: " + ex);
            }
            return customer;

        }

        /// <summary>
        /// Get a specific customer by first name and last name as parameters
        /// </summary>
        /// <param name="firstName"></param>
        /// <param name="lastName"></param>
        /// <returns>A customer</returns>
        public Customer GetCustomerByName(string firstName, string lastName)
        {
            Customer customer = new Customer();
            string sql = "SELECT CustomerId, ISNULL(FirstName, 'Empty'), ISNULL(LastName, 'Empty'), ISNULL(Country, 'Empty'), ISNULL(PostalCode, 'Empty'), ISNULL(Phone, 'Empty'), ISNULL(Email, 'Empty') FROM Customer" +
                " WHERE FirstName LIKE '%' + @FirstName + '%' AND LastName LIKE '%' + @LastName + '%'";

            try
            {
                //Connect
                using (SqlConnection conn = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    conn.Open();
                    //Make a command
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        cmd.Parameters.AddWithValue("@FirstName", firstName);
                        cmd.Parameters.AddWithValue("@LastName", lastName);

                        //Reader
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                //Handle result
                                customer.CustomerId = reader.GetInt32(0);
                                customer.FirstName = reader.GetString(1);
                                customer.LastName = reader.GetString(2);
                                customer.Country = reader.GetString(3);
                                customer.PostalCode = reader.GetString(4);
                                customer.Phone = reader.GetString(5);
                                customer.Email = reader.GetString(6);

                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine("Problem with accessing data from database: " + ex);
            }
            return customer;

        }

        /// <summary>
        /// Adds a new customer to the database
        /// </summary>
        /// <param name="customer"></param>
        /// <returns>True or false</returns>
        public bool AddNewCustomer(Customer customer)
        {

            bool success = false;
            string sql = "INSERT INTO Customer (FirstName, LastName, Country, PostalCode, Phone, Email) " +
                "VALUES(@FirstName, @LastName, @Country, @PostalCode, @Phone, @Email) ";

            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        cmd.Parameters.AddWithValue("@FirstName", customer.FirstName);
                        cmd.Parameters.AddWithValue("@LastName", customer.LastName);
                        cmd.Parameters.AddWithValue("@Country", customer.Country);
                        cmd.Parameters.AddWithValue("@PostalCode", customer.PostalCode);
                        cmd.Parameters.AddWithValue("@Phone", customer.Phone);
                        cmd.Parameters.AddWithValue("@Email", customer.Email);
                        success = cmd.ExecuteNonQuery() > 0 ? true : false;
                    }

                }

            }
            catch(Exception ex)
            {
                Console.WriteLine("Problem with adding data to database: " + ex);
            }
            return success;
        }

        /// <summary>
        /// Updates a customer's phone number, uses customerId and phone number as parameters
        /// </summary>
        /// <param name="customer"></param>
        /// <param name="phone"></param>
        /// <returns>True or false</returns>
        public bool UpdateCustomer(Customer customer, string phone)
        {
            bool success = false;
            string sql = "UPDATE Customer " +
                "SET Phone = @Phone " +
                "WHERE CustomerId = @CustomerId"; 
                
            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {

                        cmd.Parameters.AddWithValue("@Phone", phone);
                        cmd.Parameters.AddWithValue("@CustomerId", customer.CustomerId);
                        success = cmd.ExecuteNonQuery() > 0 ? true : false;
                    }

                }

            }
            catch (Exception ex)
            {
                Console.WriteLine("Problem with updating data in database: " + ex);
            }
            return success;
        }

        /// <summary>
        /// Get customers with limit and offset as parameters
        /// </summary>
        /// <param name="limit"></param>
        /// <param name="offset"></param>
        /// <returns>A list of customers</returns>
        public List<Customer> GetCostumersLimitOffset(int limit, int offset)
        {
            List<Customer> custList = new List<Customer>();
            string sql = "SELECT CustomerId, ISNULL(FirstName, 'Empty'), ISNULL(LastName, 'Empty'), ISNULL(Country, 'Empty'), ISNULL(PostalCode, 'Empty'), ISNULL(Phone, 'Empty'), ISNULL(Email, 'Empty') FROM Customer ORDER BY CustomerId OFFSET @Offset ROWS FETCH NEXT @Limit ROWS ONLY";

            try
            {
                //Connect
                using (SqlConnection conn = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    conn.Open();
                    //Make a command
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        cmd.Parameters.AddWithValue("@Limit", limit);
                        cmd.Parameters.AddWithValue("@Offset", offset);
                        //Reader
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            
                            while (reader.Read())
                            {
                                //Handle result
                                Customer temp = new Customer();

                                temp.CustomerId = reader.GetInt32(0);
                                temp.FirstName = reader.GetString(1);
                                temp.LastName = reader.GetString(2);
                                temp.Country = reader.GetString(3);
                                temp.PostalCode = reader.GetString(4);
                                temp.Phone = reader.GetString(5);
                                temp.Email = reader.GetString(6);
                                custList.Add(temp);
                            }
                        }
                    }
                }

            }
            catch (SqlException ex)
            {
                Console.WriteLine("Problem with accessing data from database: " + ex);
            }

            return custList;
        }
    }
}
