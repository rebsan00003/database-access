﻿using DatabaseAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseAccess.Repositories
{
    interface ICustomerGenreRepository
    {
        public List<CustomerGenre> GetMostPopularGenre(int id);
    }
}
