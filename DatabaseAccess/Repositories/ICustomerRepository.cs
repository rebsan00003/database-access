﻿using DatabaseAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseAccess.Repositories
{
    public interface ICustomerRepository
    {
        public Customer GetCustomerByID(int id);
        public Customer GetCustomerByName(string firstName, string lastName);

        public List<Customer> GetAllCustomers();

        public List<Customer> GetCostumersLimitOffset(int limit, int offset);


        public bool AddNewCustomer(Customer customer);

        public bool UpdateCustomer(Customer customer, string phone);


    }
}
