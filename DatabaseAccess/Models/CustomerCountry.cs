﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseAccess.Models
{
    public class CustomerCountry
    {
        public string CountryName { get; set; }
        public int NoOfCustomers { get; set; }
    }
}
