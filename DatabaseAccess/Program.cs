﻿using DatabaseAccess.Models;
using DatabaseAccess.Repositories;
using System;
using System.Collections.Generic;

namespace DatabaseAccess
{
    class Program
    {
        static void Main(string[] args)
        {
            PromptUser(); 
        }


        /// <summary>
        /// Prompts the user with different options and acts upon input
        /// </summary>
        public static void PromptUser()
        {
            ICustomerRepository repository = new CustomerRepository();
            ICustomerCountryRepository countryRepository = new CustomerCountryRepository();
            ICustomerSpenderRepository spenderRepository = new CustomerSpenderRepository();
            ICustomerGenreRepository genreRepository = new CustomerGenreRepository();
            bool run = true;

            Console.WriteLine("Welcome to Chinook! What would like to do?");

            while (run)
            {
                Console.WriteLine("\nChoose among following commands:\n1: Show all customers\n2: Show all customers with limit and offset\n" +
            "3: Select customer by ID\n4: Select customer by name\n5: Insert new customer\n6: Update customers' phone number\n7: Show number of customers per country\n" +
            "8: Show most spending customers\n9: Show a customer's most popular genre\n10: Quit program");
                string input = Console.ReadLine();

                switch (int.Parse(input))
                {
                    case 1:
                        SelectAllCustomers(repository);
                        continue;
                    case 2:
                        Console.WriteLine("Please enter a limit and an offset:");
                        string[] limitAndOffset = Console.ReadLine().Split(' ');
                        SelectLimitAndOffset(repository, int.Parse(limitAndOffset[0]), int.Parse(limitAndOffset[1]));
                        continue;
                    case 3:
                        Console.WriteLine("Please enter a customer ID:");
                        int customerId = int.Parse(Console.ReadLine());
                        SelectCustomerByID(repository, customerId);
                        continue;
                    case 4:
                        Console.WriteLine("Please enter a first name and a last name: ");
                        string[] firstAndLastName = Console.ReadLine().Split(' ');
                        SelectCustomerByName(repository, firstAndLastName[0], firstAndLastName[1]);
                        continue;
                    case 5:
                        Console.WriteLine("To add a new customer, please enter the following information in correct order: first name, last name, country, postal code, phone number, email (use comma between every input)");
                        string[] newCustomerArr = Array.ConvertAll(Console.ReadLine().Split(','), a => a.Trim());
                        InsertCustomer(repository, newCustomerArr);
                        continue;
                    case 6:
                        Console.WriteLine("To update a customer's phone number, enter the customer's ID and the new phone number (use comma between the inputs): ");
                        string[] updateCustomerNumber = Array.ConvertAll(Console.ReadLine().Split(','), a => a.Trim());
                        UpdateCustomer(repository, updateCustomerNumber);
                        continue;
                    case 7:
                        SelectCustomerCountries(countryRepository);
                        continue;
                    case 8:
                        SelectHighestSpenders(spenderRepository);
                        continue;
                    case 9:
                        Console.WriteLine("To show a customer's most popular genre, enter a customer id");
                        string id = Console.ReadLine();
                        SelectPopularGenre(genreRepository, int.Parse(id));
                        continue;
                    case 10:
                        run = false;
                        break;
                    default:
                        Console.WriteLine("Invalid input");
                        continue;
                }
            }
            
        }

        /// <summary>
        /// Call the method that selects and returns all customers from database
        /// </summary>
        /// <param name="repository"></param>
        public static void SelectAllCustomers(ICustomerRepository repository) 
        {
            PrintCustomers(repository.GetAllCustomers());
        }

        /// <summary>
        /// Call the method that selects customers from database with limit and offset
        /// </summary>
        /// <param name="repository"></param>
        public static void SelectLimitAndOffset(ICustomerRepository repository, int limit, int offset)
        {
            PrintCustomers(repository.GetCostumersLimitOffset(limit, offset));
        }

        /// <summary>
        /// Call the method that selects one customer from database based on ID
        /// </summary>
        /// <param name="repository"></param>
        public static void SelectCustomerByID(ICustomerRepository repository, int id) 
        {
            PrintCustomer(repository.GetCustomerByID(id));
        }

        /// <summary>
        /// Call the method that selects customer from database based on name
        /// </summary>
        /// <param name="repository"></param>
        public static void SelectCustomerByName(ICustomerRepository repository, string firstName, string lastName)
        {
            PrintCustomer(repository.GetCustomerByName(firstName, lastName));
        }

        /// <summary>
        /// Call the method that inserts a new customer to the database
        /// </summary>
        /// <param name="repository"></param>
        public static void InsertCustomer(ICustomerRepository repository, string[] customerArr)
        {
            Customer newCustomer = new Customer()
            {
                FirstName = customerArr[0],
                LastName = customerArr[1],
                Country = customerArr[2],
                PostalCode = customerArr[3],
                Phone = customerArr[4],
                Email = customerArr[5]
            };
            if (repository.AddNewCustomer(newCustomer))
            {
                Console.WriteLine("New customer added!");
            }
            else
            {
                Console.WriteLine("Something went wrong");
            }
        }

        /// <summary>
        /// Call the method that updates a specific customer
        /// </summary>
        /// <param name="repository"></param>
        public static void UpdateCustomer(ICustomerRepository repository, string[] updateCustomerNumber)
        {

            Customer updateCustomer = repository.GetCustomerByID(int.Parse(updateCustomerNumber[0]));

            if (repository.UpdateCustomer(updateCustomer, updateCustomerNumber[1]))
            {
                Console.WriteLine("The phone number is now updated");
                PrintCustomer(repository.GetCustomerByID(int.Parse(updateCustomerNumber[0])));
            }
            else
            {
                Console.WriteLine("Could not update phone number");
            }

        }

        /// <summary>
        /// Call the method that counts number of customers from each country
        /// </summary>
        /// <param name="countryRepository"></param>
        public static void SelectCustomerCountries(ICustomerCountryRepository countryRepository)
        {
            PrintCustomerCountries(countryRepository.GetCustomerCountries());
        }

        /// <summary>
        /// Call the method to get all customers in highest spending, descending order
        /// </summary>
        /// <param name="spenderRepository"></param>
        public static void SelectHighestSpenders(ICustomerSpenderRepository spenderRepository)
        {
            PrintHighestSpenders(spenderRepository.GetHighestSpenders());
        }

        /// <summary>
        /// Call the method to get a customer's most popular genre
        /// </summary>
        /// <param name="genreRepository"></param>
        /// <param name="id"></param>
        public static void SelectPopularGenre(ICustomerGenreRepository genreRepository, int id)
        {
            PrintMostPopularGenres(genreRepository.GetMostPopularGenre(id));
        }


        /// <summary>
        /// Print methods for the different outputs
        /// </summary>

        public static void PrintMostPopularGenres(IEnumerable<CustomerGenre> customerGenres)
        {
            foreach (CustomerGenre customerGenre in customerGenres)
            {
                PrintMostPopularGenre(customerGenre);
            }
        }
        public static void PrintMostPopularGenre(CustomerGenre customerGenre)
        {
            Console.WriteLine($"--- Genre Name: {customerGenre.GenreName} Number of Tracks: {customerGenre.NumberOfTracks}  ---");
        }

        public static void PrintHighestSpenders(IEnumerable<CustomerSpender> customerSpenders)
        {
            foreach (CustomerSpender customerSpender in customerSpenders)
            {
                PrintHighestSpender(customerSpender);
            }
        }

        public static void PrintHighestSpender(CustomerSpender customerSpender)
        {
            Console.WriteLine($"---{customerSpender.Customer.CustomerId} {customerSpender.Customer.FirstName} {customerSpender.Customer.LastName} {customerSpender.Customer.Country} {customerSpender.Customer.PostalCode} {customerSpender.Customer.Phone} {customerSpender.Customer.Email} Total spend is: {customerSpender.TotalSpend} ---");
        }


        public static void PrintCustomerCountries(IEnumerable<CustomerCountry> customerCountries)
        {
            foreach (CustomerCountry customerCountry in customerCountries)
            {
                PrintCustomerCountry(customerCountry);
            }
        }

        public static void PrintCustomerCountry(CustomerCountry customerCountry)
        {
            Console.WriteLine($"---{customerCountry.CountryName}:{customerCountry.NoOfCustomers}  ---");

        }


        public static void PrintCustomers(IEnumerable<Customer> customers)
        {
            foreach (Customer customer in customers)
            {
                PrintCustomer(customer);
            }
        }

        
        public static void PrintCustomer(Customer customer)
        {
            Console.WriteLine($"---{customer.CustomerId} {customer.FirstName} {customer.LastName} {customer.Country} {customer.PostalCode} {customer.Phone} {customer.Email} ---");
        }
        
    }
}