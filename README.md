# Database-access
This is a console application, that interacts with a chinook-database. There is functionallity to create, read and update custoumers. The user can interact with the application by choosing different options to perform actions agenst the database. 
The solution is using NuGet package SqlClient.

## Install
Clone the repository to a local directory.

```
git clone https://gitlab.com/rebsan00003/database-access.git
cd database-access

```
Install NuGet package System.Data.SqlClient



## Prerequisites


.NET Framework


Visual Studio 2017/19 OR Visual Studio Code


## Contributors
Betiel Yohannes (@betielyohannes) & Rebecka Ocampo Sandgren (@rebsan00003)

## Contributing
No contribution allowed 
